import React, { Component } from 'react';
import Routes from './routes';

class MyBatazApp extends Component {
  render() {
    return(
      <Routes />
    );
  }
}

export default MyBatazApp;

import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import SignInPage from '../components/sign-in-page';
import HomeFeed from '../components/home-feed';

export default () => (
	<BrowserRouter>
		<div>
			<Route exact path="/" component={SignInPage} />
			<Route path="/homefeed" component={HomeFeed} />
		</div>
	</BrowserRouter>
);
import React, { Component } from 'react';
import { Redirect } from 'react-router';

class SignInPage extends Component {

	constructor(props) {
		super(props);
		this.state = { userLoggedIn: false };
	}

	componentWillMount() {
		function getCookie(name) {
	      var name = name + "=";
	      var decodedCookie = decodeURIComponent(document.cookie);
	      var ca = decodedCookie.split(';');
	      for (var i = 0; i < ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0) == ' ') {
	          c = c.substring(1);
	        }
	        if (c.indexOf(name) == 0) {
	          return c.substring(name.length, c.length);
	          // return c.substring(name.length, c.length);
	        }
	      }
	    }

	    if(getCookie("name")) {
	    	var cookie_data = JSON.parse(getCookie("name"));
		}
	  
	    if(cookie_data && cookie_data.access_token) {
	    	this.state.userLoggedIn = true;
	    }
	    else {
	    	this.state.userLoggedIn = false;
	    }
	}

	render() {
		if(!this.state.userLoggedIn) {
			return(
				<div className="App">Sign In Page</div>
			);
		}
		else {
			return (
				<Redirect 
					to={{ 
						pathname: '/homefeed', 
						state: { userLoggedIn: this.state.userLoggedIn } 
					}} 
				/>);
		}
	}
}

export default SignInPage;
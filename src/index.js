import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import MybatazApp from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<MybatazApp />, document.getElementById('mybataz-app'));
registerServiceWorker();
